import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebViewScreen extends StatefulWidget {
  @override
  _WebViewScreenState createState() => _WebViewScreenState();
}

class _WebViewScreenState extends State<WebViewScreen> {
  
  @override
  Widget build(BuildContext context) {
    loadHtmlFromAssets();
    return WebviewScaffold(
      appBar: AppBar(
        title: Text("WebView"),
        backgroundColor: Colors.black,
      ),
      url: 'about:blank',
      withJavascript: true,
      allowFileURLs: true,
      withLocalUrl: true,
      withLocalStorage: true,
      withZoom: false,
      localUrlScope: 'files/',
    );
  }

  static void loadHtmlFromAssets() async {
    final flutterWebviewPlugin = new FlutterWebviewPlugin();
    String htmlText = await rootBundle.loadString('files/index.html');
    String jsText = await rootBundle.loadString('files/js/test.js');

    // ignore: unnecessary_brace_in_string_interps
    htmlText = '${htmlText}<script>${jsText}</script>';
    // flutterWebviewPlugin.evalJavascript(jsText);
    flutterWebviewPlugin.reloadUrl(Uri.dataFromString(htmlText, mimeType: 'text/html', encoding: Encoding.getByName("utf-8")).toString());
  }
}
